import date_range_picker
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
from dash.exceptions import PreventUpdate
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from calendar import monthrange

today = datetime.now()
days_in_month = monthrange(today.year, today.month)
max_date_all = today + timedelta(days=days_in_month[1]-days_in_month[0])
min_date_all = max_date_all - relativedelta(years=2)

app = dash.Dash(__name__)

app.layout = html.Div([
    html.Div([
        date_range_picker.DateRangePicker(
            id='date-range-1',
            label='date-range-1',
            className='date-range-picker-class',
            start_date=min_date_all.strftime("%Y-%m-%d"),
            end_date=max_date_all.strftime("%Y-%m-%d"),
            tooltip={
                'always_visible':True,
                'placement':'bottom'
            }
        ),
    ], style={'width':'300px', 'marginLeft':'100px', 'marginTop':'100px', "float":"left"}),
    html.Div(id='output', style={'width':'300px', 'clear':'left', 'marginTop':'5px',  "float":"left"})
])


@app.callback(Output('output', 'children'), [Input('date-range-1', 'value')])

def display_output(value):
    if value is None:
        raise PreventUpdate
    else:
        return 'You have entered {}'.format(value)


if __name__ == '__main__':
    app.run_server(debug=True)
