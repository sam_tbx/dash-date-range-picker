webpackHotUpdatedate_range_picker("main",{

/***/ "./src/lib/components/DateRangePicker.react.js":
/*!*****************************************************!*\
  !*** ./src/lib/components/DateRangePicker.react.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DateRangePicker; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-slider */ "./node_modules/rc-slider/es/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-slider/assets/index.css */ "./node_modules/rc-slider/assets/index.css");
/* harmony import */ var rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _dateRangePicker_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dateRangePicker.css */ "./src/lib/components/dateRangePicker.css");
/* harmony import */ var _dateRangePicker_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_dateRangePicker_css__WEBPACK_IMPORTED_MODULE_5__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }







/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

var DateRangePicker = /*#__PURE__*/function (_Component) {
  _inherits(DateRangePicker, _Component);

  var _super = _createSuper(DateRangePicker);

  function DateRangePicker(props) {
    var _this;

    _classCallCheck(this, DateRangePicker);

    _this = _super.call(this, props);
    _this.formatDate = _this.formatDate.bind(_assertThisInitialized(_this));
    _this.defaultDates = _this.defaultDates.bind(_assertThisInitialized(_this));
    _this.DashSlider = props.tooltip ? Object(rc_slider__WEBPACK_IMPORTED_MODULE_1__["createSliderWithTooltip"])(rc_slider__WEBPACK_IMPORTED_MODULE_1__["default"].Range) : rc_slider__WEBPACK_IMPORTED_MODULE_1__["default"].Range;
    _this.state = {
      value: props.value,
      daterange: props.daterange
    };
    return _this;
  }

  _createClass(DateRangePicker, [{
    key: "formatDate",
    value: function formatDate(date_obj) {
      var month = '' + (date_obj.getMonth() + 1);
      var day = '' + date_obj.getDate();
      var year = date_obj.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
    }
  }, {
    key: "formatDateLong",
    value: function formatDateLong(date_obj) {
      return moment__WEBPACK_IMPORTED_MODULE_3___default()(date_obj).format("MMM\xA0D\xA0YYYY");
    }
  }, {
    key: "defaultDates",
    value: function defaultDates() {
      var now = new Date();
      var twoYearsago = moment__WEBPACK_IMPORTED_MODULE_3___default()().subtract(2, 'years').toDate();
      return [this.formatDate(now), this.formatDate(twoYearsago)];
    }
  }, {
    key: "daysBetween",
    value: function daysBetween(dt1, dt2) {
      var m1 = moment__WEBPACK_IMPORTED_MODULE_3___default()(new Date(dt1));
      var m2 = moment__WEBPACK_IMPORTED_MODULE_3___default()(new Date(dt2));
      var diffInDays = m2.diff(m1, 'days');
      return diffInDays;
    }
  }, {
    key: "dayDayNumToDateLong",
    value: function dayDayNumToDateLong(num, start_date_ok) {
      var dt_obj3 = moment__WEBPACK_IMPORTED_MODULE_3___default()(start_date_ok, "YYYY-MM-DD");
      return this.formatDateLong(dt_obj3.add(num, 'days').toDate());
    }
  }, {
    key: "dayDayNumToDate",
    value: function dayDayNumToDate(num, start_date_ok) {
      var dt_obj3 = moment__WEBPACK_IMPORTED_MODULE_3___default()(start_date_ok, "YYYY-MM-DD");
      return this.formatDate(dt_obj3.add(num, 'days').toDate());
    }
  }, {
    key: "yearMarkers",
    value: function yearMarkers(num, dt1, dt2) {
      var m1 = moment__WEBPACK_IMPORTED_MODULE_3___default()(dt1, "YYYY-MM-DD");
      var m2 = moment__WEBPACK_IMPORTED_MODULE_3___default()(dt2, "YYYY-MM-DD");
      var ydiff = m2.diff(m1, 'years');
      var y1 = m1.toDate().getFullYear();
      var y2 = m2.toDate().getFullYear();
      var markers = {};

      for (var i = 1; i <= ydiff; i++) {
        var ythis = y1 + i;
        var nextyear = moment__WEBPACK_IMPORTED_MODULE_3___default()(ythis + '0101');
        var this_day_diff = moment__WEBPACK_IMPORTED_MODULE_3___default()(ythis + '0101').diff(m1, 'days');
        markers[this_day_diff] = ythis;
      }

      return markers;
    }
  }, {
    key: "datePresets",
    value: function datePresets(preset, end_date_ok) {
      var m1 = moment__WEBPACK_IMPORTED_MODULE_3___default()(end_date_ok, "YYYY-MM-DD");

      if (preset == '1D') {
        var start_date = this.formatDate(m1.subtract(1, 'day').toDate());
        var daysBetween = this.daysBetween(end_date_ok, start_date);
        return [end_date_ok, start_date, daysBetween];
      } else if (preset == '1W') {
        var _start_date = this.formatDate(m1.subtract(7, 'days').toDate());

        var _daysBetween = this.daysBetween(end_date_ok, _start_date);

        return [end_date_ok, _start_date, _daysBetween];
      } else if (preset == '1M') {
        var _start_date2 = this.formatDate(m1.subtract(1, 'months').toDate());

        var _daysBetween2 = this.daysBetween(end_date_ok, _start_date2);

        return [end_date_ok, _start_date2, _daysBetween2];
      } else if (preset == '3M') {
        var _start_date3 = this.formatDate(m1.subtract(3, 'months').toDate());

        var _daysBetween3 = this.daysBetween(end_date_ok, _start_date3);

        return [end_date_ok, _start_date3, _daysBetween3];
      } else if (preset == '6M') {
        var _start_date4 = this.formatDate(m1.subtract(6, 'months').toDate());

        var _daysBetween4 = this.daysBetween(end_date_ok, _start_date4);

        return [end_date_ok, _start_date4, _daysBetween4];
      } else {
        var _start_date5 = this.formatDate(m1.subtract(1, 'years').toDate());

        var _daysBetween5 = this.daysBetween(end_date_ok, _start_date5);

        return [end_date_ok, _start_date5, _daysBetween5];
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          id = _this$props.id,
          label = _this$props.label,
          className = _this$props.className,
          tooltip = _this$props.tooltip,
          start_date = _this$props.start_date,
          end_date = _this$props.end_date,
          setProps = _this$props.setProps;
      var value = this.state.value;
      var daterange = this.state.daterange;
      var tipProps;

      if (tooltip && tooltip.always_visible) {
        /**
         * clone `tooltip` but with renamed key `always_visible` -> `visible`
         * the rc-tooltip API uses `visible`, but `always_visible` is more semantic
         * assigns the new (renamed) key to the old key and deletes the old key
         */
        tipProps = assoc('visible', tooltip.always_visible, tooltip);
        delete tipProps.always_visible;
      } else {
        tipProps = tooltip;
      }

      var defaultDates = this.defaultDates();
      var start_date_ok;
      var end_date_ok;

      if (start_date == undefined) {
        // Youngest date
        start_date_ok = defaultDates[1];
      } else {
        start_date_ok = start_date;
      }

      if (end_date == undefined) {
        // Oldest date
        end_date_ok = defaultDates[0];
      } else {
        end_date_ok = end_date;
      }

      var daysBetween = this.daysBetween(start_date_ok, end_date_ok);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: id,
        className: (className, 'dateRangePicker_slider')
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(this.DashSlider, {
        range: true,
        onChange: function onChange(value) {
          _this2.setState({
            value: value
          });
        },
        onAfterChange: function onAfterChange(value) {
          _this2.setState({
            value: value,
            daterange: [_this2.dayDayNumToDate(value[0], start_date_ok), _this2.dayDayNumToDate(value[1], start_date_ok)]
          });
        },
        marks: this.yearMarkers(daysBetween, start_date_ok, end_date_ok),
        min: 0,
        max: daysBetween,
        defaultValue: [0, daysBetween],
        tipFormatter: function tipFormatter(value) {
          return "".concat(_this2.dayDayNumToDateLong(value, start_date_ok));
        },
        tipProps: {
          placement: "top",
          visible: true
        },
        value: this.state.value
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "dateRangePicker_button_range"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('1D', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "1D"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('1W', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "1W"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('1M', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "1M"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('3M', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "3M"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('6M', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "6M"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_button",
        onClick: function onClick(e) {
          var preset_dt = _this2.datePresets('1Y', end_date_ok);

          _this2.setState({
            value: [daysBetween + preset_dt[2], daysBetween],
            daterange: [preset_dt[1], preset_dt[0]]
          });
        }
      }, "1Y"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "dateRangePicker_submit",
        onClick: function onClick(e) {
          return setProps({
            value: _this2.state.daterange
          });
        }
      }, "Filter")));
    }
  }]);

  return DateRangePicker;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


DateRangePicker.defaultProps = {};
DateRangePicker.propTypes = {
  /**
   * The ID used to identify this component in Dash callbacks.
   */
  id: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * A label that will be printed when this component is rendered.
   */
  label: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * The value displayed in the input.
   */
  className: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  tooltip: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a["boolean"],
  start_date: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  end_date: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * Dash-assigned callback that should be called to report property changes
   * to Dash, to make them available for callbacks.
   */
  setProps: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func
};

/***/ })

})
//# sourceMappingURL=6c3e441-main-wps-hmr.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiI2YzNlNDQxLW1haW4td3BzLWhtci5qcyIsInNvdXJjZVJvb3QiOiIifQ==