webpackHotUpdatedate_range_picker("main",{

/***/ "./src/lib/components/DateRangePicker.react.js":
/*!*****************************************************!*\
  !*** ./src/lib/components/DateRangePicker.react.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DateRangePicker; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-slider */ "./node_modules/rc-slider/es/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-slider/assets/index.css */ "./node_modules/rc-slider/assets/index.css");
/* harmony import */ var rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rc_slider_assets_index_css__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */

var DateRangePicker = /*#__PURE__*/function (_Component) {
  _inherits(DateRangePicker, _Component);

  var _super = _createSuper(DateRangePicker);

  function DateRangePicker(props) {
    var _this;

    _classCallCheck(this, DateRangePicker);

    _this = _super.call(this, props);
    _this.formatDate = _this.formatDate.bind(_assertThisInitialized(_this));
    _this.defaultDates = _this.defaultDates.bind(_assertThisInitialized(_this));
    _this.DashSlider = props.tooltip ? Object(rc_slider__WEBPACK_IMPORTED_MODULE_1__["createSliderWithTooltip"])(rc_slider__WEBPACK_IMPORTED_MODULE_1__["default"].Range) : rc_slider__WEBPACK_IMPORTED_MODULE_1__["default"].Range;
    _this.state = {
      value: props.value
    };
    return _this;
  }

  _createClass(DateRangePicker, [{
    key: "formatDate",
    value: function formatDate(date_obj) {
      var month = '' + (date_obj.getMonth() + 1);
      var day = '' + date_obj.getDate();
      var year = date_obj.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
    }
  }, {
    key: "defaultDates",
    value: function defaultDates() {
      var now = new Date();
      var twoYearsago = new Date(now.setFullYear(now.getFullYear() - 2));
      return [this.formatDate(now), this.formatDate(twoYearsago)];
    }
  }, {
    key: "daysBetween",
    value: function daysBetween(dt1, dt2) {
      var dt1_obj = new Date(dt1);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          id = _this$props.id,
          label = _this$props.label,
          className = _this$props.className,
          tooltip = _this$props.tooltip,
          start_date = _this$props.start_date,
          end_date = _this$props.end_date;
      var value = this.state.value;
      var tipProps;

      if (tooltip && tooltip.always_visible) {
        /**
         * clone `tooltip` but with renamed key `always_visible` -> `visible`
         * the rc-tooltip API uses `visible`, but `always_visible` is more semantic
         * assigns the new (renamed) key to the old key and deletes the old key
         */
        tipProps = assoc('visible', tooltip.always_visible, tooltip);
        delete tipProps.always_visible;
      } else {
        tipProps = tooltip;
      }

      var defaultDates = this.defaultDates();
      var start_date_ok;
      var end_date_ok;

      if (start_date == undefined) {
        start_date_ok = defaultDates[0];
      } else {
        start_date_ok = start_date;
      }

      if (end_date == undefined) {
        end_date_ok = defaultDates[1];
      } else {
        end_date_ok = end_date;
      }

      var daysBetween = this.daysBetween(start_date_ok, end_date_ok);
      var truncatedMarks = this.props.marks ? pickBy(function (k, mark) {
        return mark >= _this2.props.min && mark <= _this2.props.max;
      }, this.props.marks) : this.props.marks;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: id,
        className: className
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(this.DashSlider, {
        range: true,
        onChange: function onChange(value) {
          _this2.setState({
            value: value
          });

          console.log(value);
        },
        onAfterChange: function onAfterChange(value) {
          console.log(value);
        },
        marks: {
          100: start_date_ok,
          500: end_date_ok
        },
        min: 0,
        max: 500,
        defaultValue: [200, 300],
        tipFormatter: function tipFormatter(value) {
          return "$ ".concat(value);
        },
        tipProps: {
          placement: "top",
          visible: true
        },
        value: value
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", null, "Set Date"));
    }
  }]);

  return DateRangePicker;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


DateRangePicker.defaultProps = {};
DateRangePicker.propTypes = {
  /**
   * The ID used to identify this component in Dash callbacks.
   */
  id: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * A label that will be printed when this component is rendered.
   */
  label: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * The value displayed in the input.
   */
  className: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  tooltip: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a["boolean"],
  start_date: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  end_date: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,

  /**
   * Dash-assigned callback that should be called to report property changes
   * to Dash, to make them available for callbacks.
   */
  setProps: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func
};

/***/ })

})
//# sourceMappingURL=7ae2f7b-main-wps-hmr.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiI3YWUyZjdiLW1haW4td3BzLWhtci5qcyIsInNvdXJjZVJvb3QiOiIifQ==