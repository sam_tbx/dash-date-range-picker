# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class DateRangePicker(Component):
    """A DateRangePicker component.


Keyword arguments:

- id (string; optional):
    The ID used to identify this component in Dash callbacks.

- className (string; optional):
    The value displayed in the input.

- end_date (string; optional)

- label (string; optional):
    A label that will be printed when this component is rendered.

- loading_state (dict; optional)

    `loading_state` is a dict with keys:

    - component_name (string; optional):
        Holds the name of the component that is loading.

    - is_loading (boolean; optional):
        Determines if the component is loading or not.

    - prop_name (string; optional):
        Holds which property is loading.

- persistence (boolean | string | number; optional):
    Used to allow user interactions in this component to be persisted
    when the component - or the page - is refreshed. If `persisted` is
    truthy and hasn't changed from its previous value, a `value` that
    the user has changed while using the app will keep that change, as
    long as the new `value` also matches what was given originally.
    Used in conjunction with `persistence_type`.

- persistence_type (a value equal to: 'local', 'session', 'memory'; default 'local'):
    Where persisted user changes will be stored: memory: only kept in
    memory, reset on page refresh. local: window.localStorage, data is
    kept after the browser quit. session: window.sessionStorage, data
    is cleared once the browser quit.

- start_date (string; optional)

- tooltip (dict; optional)

    `tooltip` is a dict with keys:

    - always_visible (boolean; optional):
        Determines whether tooltips should always be visible (as
        opposed to the default, visible on hover).

    - placement (a value equal to: 'left', 'right', 'top', 'bottom', 'topLeft', 'topRight', 'bottomLeft', 'bottomRight'; optional):
        Determines the placement of tooltips See
        https://github.com/react-component/tooltip#api top/bottom{*}
        sets the _origin_ of the tooltip, so e.g. `topLeft` will in
        reality appear to be on the top right of the handle.

- value (list of strings; optional)"""
    @_explicitize_args
    def __init__(self, id=Component.UNDEFINED, label=Component.UNDEFINED, className=Component.UNDEFINED, value=Component.UNDEFINED, tooltip=Component.UNDEFINED, start_date=Component.UNDEFINED, end_date=Component.UNDEFINED, loading_state=Component.UNDEFINED, persistence=Component.UNDEFINED, persistence_type=Component.UNDEFINED, **kwargs):
        self._prop_names = ['id', 'className', 'end_date', 'label', 'loading_state', 'persistence', 'persistence_type', 'start_date', 'tooltip', 'value']
        self._type = 'DateRangePicker'
        self._namespace = 'date_range_picker'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['id', 'className', 'end_date', 'label', 'loading_state', 'persistence', 'persistence_type', 'start_date', 'tooltip', 'value']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}
        for k in []:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(DateRangePicker, self).__init__(**args)
