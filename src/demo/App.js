/* eslint no-magic-numbers: 0 */
import React, {Component} from 'react';

import { DateRangePicker } from '../lib';


class App extends Component {

    constructor() {
        super();
        this.state = {
            // start_date:default_dates[1],
            // end_date:default_dates[0],
            tooltip: {
              always_visible:true
            }
        };
        this.setProps = this.setProps.bind(this);
    }


    setProps(newProps) {
        this.setState(newProps);
    }

    render() {
        return (
            <div
              style={{'width':'300px', 'marginLeft':'100px', 'marginTop':'100px'}}
            >
                <DateRangePicker
                    setProps={this.setProps}
                    {...this.state}
                />
            </div>
        )
    }
}

export default App;
