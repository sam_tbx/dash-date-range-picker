import React, {Component} from 'react';
import ReactSlider, {createSliderWithTooltip} from 'rc-slider';
import PropTypes from 'prop-types';
import {assoc, omit, pickBy} from 'ramda';
import moment from 'moment'
import 'rc-slider/assets/index.css';
import './dateRangePicker.css';
/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */

Date.prototype.addDays = function(days) {
 let date = new Date(this.valueOf());
 date.setDate(date.getDate() + days);
 return date;
}

export default class DateRangePicker extends Component {

    constructor(props) {
        super(props);
        this.formatDate = this.formatDate.bind(this);
        this.defaultDates = this.defaultDates.bind(this);
        this.DashSlider = props.tooltip
            ? createSliderWithTooltip(ReactSlider.Range)
            : ReactSlider.Range;
        this.state = {value: props.value, daterange:props.daterange, data:props.data};
    }

    formatDate(date_obj) {
        let month = '' + (date_obj.getMonth() + 1)
        let day = '' + date_obj.getDate()
        let year = date_obj.getFullYear()

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    formatDateLong(date_obj) {
      return moment(date_obj).format("MMM\u00a0D\u00a0YYYY")
    }

    defaultDates() {
      let now = new Date()
      let twoYearsago = moment().subtract(2, 'years').toDate()
      return [this.formatDate(now), this.formatDate(twoYearsago)]
    }

    daysBetween(dt1, dt2) {
      let m1 = moment(new Date(dt1))
      let m2 = moment(new Date(dt2))
      let diffInDays = m2.diff(m1,'days')

      return diffInDays;
    }

    dayDayNumToDateLong(num, start_date_ok){
      let dt_obj3 = moment(start_date_ok, "YYYY-MM-DD")
      return this.formatDateLong(dt_obj3.add(num, 'days').toDate())
    }

    dayDayNumToDate(num, start_date_ok){
      let dt_obj3 = moment(start_date_ok, "YYYY-MM-DD")
      return this.formatDate(dt_obj3.add(num, 'days').toDate())
    }

    yearMarkers(num, dt1, dt2){

      let m1 = moment(dt1, "YYYY-MM-DD")
      let m2 = moment(dt2, "YYYY-MM-DD")
      let ydiff = m2.diff(m1,'years')

      let y1 = m1.toDate().getFullYear()
      let y2 = m2.toDate().getFullYear()

      let markers = {}

      for (let i = 1; i <= ydiff; i++) {
        let ythis = y1+i
        let nextyear = moment(ythis+'0101')
        let this_day_diff = moment(ythis+'0101').diff(m1, 'days')
        markers[this_day_diff] = ythis

      }
      return markers
    }

    datePresets(preset, end_date_ok){

      let m1 = moment(end_date_ok, "YYYY-MM-DD")
      if (preset == '1D') {
        let start_date = this.formatDate(m1.subtract(1, 'day').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      } else if (preset == '1W') {
        let start_date = this.formatDate(m1.subtract(7, 'days').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      } else if (preset == '1M') {
        let start_date = this.formatDate(m1.subtract(1, 'months').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      } else if (preset == '3M') {
        let start_date = this.formatDate(m1.subtract(3, 'months').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      } else if (preset == '6M') {
        let start_date = this.formatDate(m1.subtract(6, 'months').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      } else {
        let start_date = this.formatDate(m1.subtract(1, 'years').toDate())
        let daysBetween = this.daysBetween(end_date_ok, start_date)
        return [end_date_ok, start_date, daysBetween]
      }

    }


    render() {
      const {
          id,
          label,
          className,
          loading_state,
          tooltip,
          start_date,
          end_date,
          setProps
      } = this.props;
      const value = this.state.value;
      const data = this.state.data;
      const daterange = this.state.daterange;

        let tipProps;
        if (tooltip && tooltip.always_visible) {
            /**
             * clone `tooltip` but with renamed key `always_visible` -> `visible`
             * the rc-tooltip API uses `visible`, but `always_visible` is more semantic
             * assigns the new (renamed) key to the old key and deletes the old key
             */
            tipProps = assoc('visible', tooltip.always_visible, tooltip);
            delete tipProps.always_visible;
        } else {
            tipProps = tooltip;
        }

        const defaultDates = this.defaultDates()

        let start_date_ok;
        let end_date_ok;
        if(start_date == undefined){ // Youngest date
          start_date_ok = defaultDates[1]
        } else {
          start_date_ok = start_date
        }

        if(end_date == undefined){ // Oldest date
          end_date_ok = defaultDates[0]
        } else {
          end_date_ok = end_date
        }
        const daysBetween = this.daysBetween(start_date_ok, end_date_ok)

        return (

          <div
            id={id}
            className={className, 'dateRangePicker_wrapper'}
            data-dash-is-loading={
                (loading_state && loading_state.is_loading) || undefined
            }
            data={this.state.data}
          >
            <this.DashSlider
                className={'dateRangePicker_slider'}
                range={true}
                onChange={value => {
                  this.setState({value: value})
                }}
                onAfterChange={value => {
                    this.setState({value: value,
                      data:value,
                      daterange:[
                        this.dayDayNumToDate(value[0], start_date_ok),
                        this.dayDayNumToDate(value[1], start_date_ok)
                      ]})
                }}
                marks={this.yearMarkers(daysBetween, start_date_ok, end_date_ok)}
                min={0}
                max={daysBetween}
                defaultValue={[0, daysBetween]}
                tipFormatter={value => `${this.dayDayNumToDateLong(value, start_date_ok)}`}
                tipProps={{
                  placement: tooltip.placement,
                  visible: true
                }}
                value={this.state.value}
                {...omit(
                    [
                        'className',
                        'setProps',
                        'value',
                        'marks'
                    ],
                    this.props
                )}
            />
          <div
            className="dateRangePicker_button_range"
          >
          <button
            className={"dateRangePicker_button"}
            onClick={e => {
              let preset_dt = this.datePresets('1D', end_date_ok)
              this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                daterange:[
                  preset_dt[1],
                  preset_dt[0]
                ]})
            }}
          >
            {"1D"}
          </button>
          <button
            className={"dateRangePicker_button"}
            onClick={e => {
              let preset_dt = this.datePresets('1W', end_date_ok)
              this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                daterange:[
                  preset_dt[1],
                  preset_dt[0]
                ]})
            }}
          >
            {"1W"}
          </button>
          <button
            className={"dateRangePicker_button"}
            onClick={e => {
              let preset_dt = this.datePresets('1M', end_date_ok)
              this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                data:[daysBetween+preset_dt[2],daysBetween],
                daterange:[
                  preset_dt[1],
                  preset_dt[0]
                ]})
            }}
          >
            {"1M"}
          </button>
          <button
            className={"dateRangePicker_button"}
            onClick={e => {
              let preset_dt = this.datePresets('3M', end_date_ok)
              this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                data:[daysBetween+preset_dt[2],daysBetween],
                daterange:[
                  preset_dt[1],
                  preset_dt[0]
                ]})
            }}
          >
            {"3M"}
          </button>
          <button
            className={"dateRangePicker_button"}
            onClick={e => {
              let preset_dt = this.datePresets('6M', end_date_ok)
              this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                data:[daysBetween+preset_dt[2],daysBetween],
                daterange:[
                  preset_dt[1],
                  preset_dt[0]
                ]})
            }}
          >
            {"6M"}
          </button>
            <button
              className={"dateRangePicker_button"}
              onClick={e => {
                let preset_dt = this.datePresets('1Y', end_date_ok)
                this.setState({value: [daysBetween+preset_dt[2],daysBetween],
                  data:[daysBetween+preset_dt[2],daysBetween],
                  daterange:[
                    preset_dt[1],
                    preset_dt[0]
                  ]})
              }}
            >
              {"1Y"}
            </button>
            <button
              className={"dateRangePicker_submit"}
              onClick={e => setProps({
                  value: this.state.daterange,
                  data: this.state.data
                })}
            >
              {"Filter"}
            </button>
            </div>
          </div>
        );
    }
}

DateRangePicker.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * A label that will be printed when this component is rendered.
     */
    label: PropTypes.string,

    /**
     * The value displayed in the input.
     */

    className: PropTypes.string,

    value: PropTypes.arrayOf(PropTypes.string),

    tooltip: PropTypes.exact({
        /**
         * Determines whether tooltips should always be visible
         * (as opposed to the default, visible on hover)
         */
        always_visible: PropTypes.bool,

        /**
         * Determines the placement of tooltips
         * See https://github.com/react-component/tooltip#api
         * top/bottom{*} sets the _origin_ of the tooltip, so e.g. `topLeft`
         * will in reality appear to be on the top right of the handle
         */
        placement: PropTypes.oneOf([
            'left',
            'right',
            'top',
            'bottom',
            'topLeft',
            'topRight',
            'bottomLeft',
            'bottomRight',
        ]),
    }),
    start_date: PropTypes.string,
    end_date: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func,
    loading_state: PropTypes.shape({
        /**
         * Determines if the component is loading or not
         */
        is_loading: PropTypes.bool,
        /**
         * Holds which property is loading
         */
        prop_name: PropTypes.string,
        /**
         * Holds the name of the component that is loading
         */
        component_name: PropTypes.string,
    }),

    /**
     * Used to allow user interactions in this component to be persisted when
     * the component - or the page - is refreshed. If `persisted` is truthy and
     * hasn't changed from its previous value, a `value` that the user has
     * changed while using the app will keep that change, as long as
     * the new `value` also matches what was given originally.
     * Used in conjunction with `persistence_type`.
     */
    persistence: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.string,
        PropTypes.number,
    ]),

    /**
     * Properties whose user interactions will persist after refreshing the
     * component or the page. Since only `value` is allowed this prop can
     * normally be ignored.
     */

    /**
     * Where persisted user changes will be stored:
     * memory: only kept in memory, reset on page refresh.
     * local: window.localStorage, data is kept after the browser quit.
     * session: window.sessionStorage, data is cleared once the browser quit.
     */
    persistence_type: PropTypes.oneOf(['local', 'session', 'memory']),

};

DateRangePicker.defaultProps = {
  persistence_type: 'local',
};

export const propTypes = DateRangePicker.propTypes;
export const defaultProps = DateRangePicker.defaultProps;
